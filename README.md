# LoRaWAN Based Relay System

 A relay system is installed between the relay gateway and the LoRaWAN gateway. This system is basically used to avoid internet usage on the relay side. Here we have a relay gateway that receives the signal from the nearest edge devices and sends a downlink. The downlink carries the payload and credentials of the received node. The relay node receives this downlink and signals to the main gateway.

 ## Getting Started


- Make sure that you have a STM32 based Board.

- Install LoRaWAN software expansion code from [here](https://www.st.com/en/embedded-software/i-cube-lrwan.html) . Copy the code to ~/STM32CubeIDE/workspace/

- Select board : B-L072Z-LRWAN1 Development Board

## Prerequisites

STM32CubeIDE 1.1.0[Tested]

## License

This project is licensed under the MIT License - see the [LICENSE.md](https://gitlab.com/arunchandra2500/lorawan-based-relay-system/-/blob/master/LICENSE) file for details
