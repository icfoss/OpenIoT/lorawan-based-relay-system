################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (10.3-2021.10)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Drivers/Middlewares/LoRaWAN/Utilities/low_power_manager.c \
../Drivers/Middlewares/LoRaWAN/Utilities/queue.c \
../Drivers/Middlewares/LoRaWAN/Utilities/systime.c \
../Drivers/Middlewares/LoRaWAN/Utilities/timeServer.c \
../Drivers/Middlewares/LoRaWAN/Utilities/trace.c \
../Drivers/Middlewares/LoRaWAN/Utilities/utilities.c 

OBJS += \
./Drivers/Middlewares/LoRaWAN/Utilities/low_power_manager.o \
./Drivers/Middlewares/LoRaWAN/Utilities/queue.o \
./Drivers/Middlewares/LoRaWAN/Utilities/systime.o \
./Drivers/Middlewares/LoRaWAN/Utilities/timeServer.o \
./Drivers/Middlewares/LoRaWAN/Utilities/trace.o \
./Drivers/Middlewares/LoRaWAN/Utilities/utilities.o 

C_DEPS += \
./Drivers/Middlewares/LoRaWAN/Utilities/low_power_manager.d \
./Drivers/Middlewares/LoRaWAN/Utilities/queue.d \
./Drivers/Middlewares/LoRaWAN/Utilities/systime.d \
./Drivers/Middlewares/LoRaWAN/Utilities/timeServer.d \
./Drivers/Middlewares/LoRaWAN/Utilities/trace.d \
./Drivers/Middlewares/LoRaWAN/Utilities/utilities.d 


# Each subdirectory must supply rules for building sources it contributes
Drivers/Middlewares/LoRaWAN/Utilities/%.o Drivers/Middlewares/LoRaWAN/Utilities/%.su: ../Drivers/Middlewares/LoRaWAN/Utilities/%.c Drivers/Middlewares/LoRaWAN/Utilities/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m0plus -std=gnu11 -g3 -DUSE_HAL_DRIVER -DDEBUG -DSTM32L072xx -DUSE_B_L072Z_LRWAN1 -DREGION_IN865 -DLOW_POWER_DISABLE -c -I../Drivers/CMSIS/Device/ST/STM32L0xx/Include -I../Drivers/CMSIS/Include -I../Drivers/STM32L0xx_HAL_Driver/Inc -I../App/Core/Inc -I../App/LoRaWAN/inc -I../Drivers/BSP/CMWX1ZZABZ-0xx -I../Drivers/Middlewares/LoRaWAN/Crypto -I../Drivers/Middlewares/LoRaWAN/Mac -I../Drivers/Middlewares/LoRaWAN/Mac/region -I../Drivers/Middlewares/LoRaWAN/Phy -I../Drivers/Middlewares/LoRaWAN/Utilities -I../Drivers/BSP/B-L072Z-LRWAN1 -I../Drivers/BSP/Components/sx1276 -I../Drivers/Middlewares/LoRaWAN/Patterns/Basic -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"

clean: clean-Drivers-2f-Middlewares-2f-LoRaWAN-2f-Utilities

clean-Drivers-2f-Middlewares-2f-LoRaWAN-2f-Utilities:
	-$(RM) ./Drivers/Middlewares/LoRaWAN/Utilities/low_power_manager.d ./Drivers/Middlewares/LoRaWAN/Utilities/low_power_manager.o ./Drivers/Middlewares/LoRaWAN/Utilities/low_power_manager.su ./Drivers/Middlewares/LoRaWAN/Utilities/queue.d ./Drivers/Middlewares/LoRaWAN/Utilities/queue.o ./Drivers/Middlewares/LoRaWAN/Utilities/queue.su ./Drivers/Middlewares/LoRaWAN/Utilities/systime.d ./Drivers/Middlewares/LoRaWAN/Utilities/systime.o ./Drivers/Middlewares/LoRaWAN/Utilities/systime.su ./Drivers/Middlewares/LoRaWAN/Utilities/timeServer.d ./Drivers/Middlewares/LoRaWAN/Utilities/timeServer.o ./Drivers/Middlewares/LoRaWAN/Utilities/timeServer.su ./Drivers/Middlewares/LoRaWAN/Utilities/trace.d ./Drivers/Middlewares/LoRaWAN/Utilities/trace.o ./Drivers/Middlewares/LoRaWAN/Utilities/trace.su ./Drivers/Middlewares/LoRaWAN/Utilities/utilities.d ./Drivers/Middlewares/LoRaWAN/Utilities/utilities.o ./Drivers/Middlewares/LoRaWAN/Utilities/utilities.su

.PHONY: clean-Drivers-2f-Middlewares-2f-LoRaWAN-2f-Utilities

